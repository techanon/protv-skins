# ArchiTech.ProTV.Extras Content

## Blu UI
#### Contributed by BluWizard
![Blu UI screencap](.README~/BluUI.png)

#### Custom Features
- Custom Standby Image
- Marquee Scrolling Title Info

#### Standard Features:
- Standard inputs: Main URL/Alternate URL/Title
- Standard buttons: Play/Pause/Stop/Reload/Resync/QuickSeek
- Standard toggles: Lock/Audio Mode/Color Correction/Loop/3D Width
- Standard sliders: Volume/Seek/Playback Speed/Seek Offset
- Standard dropdowns: Video Engine Select/3D Mode
- Standard displays: Current Time/Media Duration/Title Info

## RiskiPlayer
#### Contributed by RiskiVR
![RiskiPlayer screencap](.README~/RiskiPlayer.png)

#### Custom Features
- Custom Standby Image
- UI animation transitions
- Screen Brightness Slider

#### Standard Features
- Standard inputs: Main URL
- Standard buttons: Play/Pause/Reload/Resync
- Standard toggles: Lock/Loop
- Standard sliders: Volume/Seek
- Standard displays: Current Time/Media Duration

## Cyber Blue UI
### Contributed by BluWizard
![CyberBlue UI Screencap](.README~/CyberBlue.png)

#### Custom Features
- Custom Standby Image

#### Standard Features
- Standard inputs: Main URL
- Standard buttons: Play/Pause/Stop/Reload/Resync/Skip
- Standard toggles: Lock
- Standard slider: Volume/Seek
- Standard dropdowns: Video Engine Select
- Standard displays: Current Time/Media Duration/Title Info

## Cyber Red UI
### Contributed by BluWizard
![CyberRed UI Screencap](.README~/CyberRed.png)

#### Custom Features
- Custom Standby Image

#### Standard Features:
- Standard inputs: Main URL/Alternate URL/Title
- Standard buttons: Play/Pause/Stop/Reload/Resync/QuickSeek
- Standard toggles: Lock/Audio Mode/Color Correction/Loop
- Standard sliders: Volume/Seek/Playback Speed/Seek Offset
- Standard dropdowns: Video Engine Select
- Standard displays: Current Time/Title Info
